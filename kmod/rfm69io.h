/* rfm69io.h - kernel module, RFM69 input output header file

Copyright (C) 2016  W. Hein, G. Kraut

This file is part of rfm69gmsk.

rfm69gmsk is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

rfm69gmsk is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rfm69gmsk.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __RFM69IO_H__
#define __RFM69IO_H__

/* ----- Some definitions ---------------------------------------------------*/

#define CHANNELS	35		/* number of 200kHz channels in 963-970MHz band */
#define TRANSFERNBR	7		/* maximum number of transfers in a message	*/

#define IOBUFLEN	128		/* maximum size of the RFM69 SPI iobuf		*/
#define DBADDR		63		/* position in iobuf for FIFO's SPI address	*/
#define DBID		64		/* LID/RID position in iobuf to/from FIFO	*/
#define DBPL		68		/* Payload position in iobuf to/from FIFO	*/
/* IOBUF is one buffer for SPI io with RFM69 subdivided into following subsections:
 * 0 - 62 .... 63 bytes, for SPI traffic to/from normal RFM69 registers
 * 63 ........ 1 byte, for FIFO's SPI register address
 * 64 - 67 ... 4 bytes, local ID in to be transmitted or remote ID in received blocks
 * 68 - 127 .. 60 bytes, for payload beyond local ID					*/

/* ----- Function declarations ---------------------------------------------- */

int rfm69_hwinit(struct spi_device *);
int rfm69_transmit(struct spi_device *);
int rfm69_receive(struct spi_device *);
#endif