/* rfm69io.c - kernel module, RFM69 input output routines

Copyright (C) 2016  W. Hein, G. Kraut

This file is part of rfm69gmsk.

rfm69gmsk is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

rfm69gmsk is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rfm69gmsk.  If not, see <http://www.gnu.org/licenses/>. */

#include <linux/errno.h>
#include <linux/spi/spi.h>

#include "rfm69gmsk.h"
#include "rfm69io.h"

/* ----- Macros definitions ------------------------------------------------ */
#define	CONT			1	/* cs_change for continuous transfer		*/
#define LAST			0	/* cs_change for last transfer in a message	*/

#define CFIFOWR(addr,length,trfp,sequ,iop,i) {	\
	(trfp)->tx_buf = (iop);				\
	(trfp)->rx_buf = (iop);				\
	(trfp)->len = (length)+1;			\
	(trfp)->cs_change = (sequ);			\
	*(iop)++ = (addr) | 0x80;			\
	iop += length;					\
	(trfp)++;					\
}

#define CCMDWR(addr,val,length,trfp,sequ,iop,i) {	\
	(trfp)->tx_buf = (iop);				\
	(trfp)->rx_buf = (iop);				\
	(trfp)->len = (length)+1;			\
	(trfp)->cs_change = (sequ);			\
	*(iop)++ = (addr) | 0x80;			\
	for ((i) = (length)-1; (i) >= 0; (i)--)		\
		*(iop)++ = ((val) >> i*8) & 0xFF;	\
	(trfp)++;					\
}

#define CCMDRD(addr,length,trfp,sequ,iop,i) {		\
	(trfp)->tx_buf = (iop);				\
	(trfp)->rx_buf = (iop);				\
	(trfp)->len = (length)+1;			\
	(trfp)->cs_change = (sequ);			\
	*(iop)++ = (addr) & 0x7F;			\
	for ((i) = (length)-1; (i) >= 0; (i)--)		\
		*(iop)++ = 0;				\
	(trfp)++;					\
}

#define WRBUF(val,iop,length,i) {			\
	for ((i) = (length)-1; (i) >= 0; (i)--)		\
		*(iop)++ = ((val) >> i*8) & 0xFF;	\
}

#define RDBUF(iop,val,length,i) {			\
	(val) = 0;					\
	for ((i) = (length)-1; (i) >= 0; (i)--)		\
		(val) |= *(iop)++ << i*8;		\
}

/* ----- Common register definitions --------------------------------------- */
#define RFM69_REGLEN		1	/* length of a standard register		*/

#define RFM69_FIFO		0x00	/* FIFO access register				*/
#define RFM69_FIFOLEN		66

#define RFM69_MODE		0x01
#define RFM69_DPMOD		0x02	/* data processing mode and modulation		*/

#define RFM69_BITRATE		0x03	/* Bit rate, (15:0) = MSB(0x03) + LSB(0x04)	*/
#define RFM69_BITRATELEN	2

#define RFM69_FREQDEV		0x05	/* Frequency deviation, (13:0) = MSB(0x05) + LSB(0x06)	*/
#define RFM69_FREQDEVLEN	2

#define RFM69_FREQC		0x07	/* Carrier Fc, (23:0) = MSB(0x07) ... LSB(0x09)	*/
#define RFM69_FREQCLEN		3

#define RFM69_RCCAL		0x0A	/* RC Calibration, (7:6)			*/
#define RFM69_AFCLOWMOD		0x0B	/* AFC low mod index impovement on/off control (5)	*/
#define RFM69_LOWBAT		0x0C	/* Low Battery control and reporting (4:0)	*/

#define RFM69_LSTNCTRL		0x0D	/* Listen control				*/
#define RFM69_LSTNTIMEIDLE	0x0E	/* Listen time Idle				*/
#define RFM69_LSTNGIMERX	0x0F	/* Listen time Rx				*/
#define RFM69_CHIPVER		0x10	/* Chip version code				*/

/* ----- Tx register definitions ------------------------------------------- */
#define RFM69_PALEVEL		0x11	/* PA output power level [dBm]			*/
#define RFM69_PARAMP		0x12	/* FSK PA rise/fall ramp period (3:0)		*/

/* ----- Rx register definitions ------------------------------------------- */
#define RFM69_LNACTRL		0x18	/* LNA control					*/
#define RFM69_RXBW		0x19	/* Rx bandwidth					*/
#define RFM69_AFCBW		0x1A	/* AFC bandwidth				*/

#define RFM69_AFCFEICTRL	0x1E	/* AFC and FEI control				*/

#define RFM69_AFCVAL		0x1F	/* AFC reading, (15:0) = MSB(0x1F) + LSB(0x20)	*/
#define RFM69_AFCVALLEN		2

#define RFM69_FEIVAL		0x21	/* FEI reading, (15:0) = MSB(0x21) + LSB(0x22)	*/
#define RFM69_FEIVALLEN		2

#define RFM69_RSSICTRL		0x23	/* RSSI measurement control, (1:0)		*/
#define RFM69_RSSIVAL		0x24	/* RSSI reading, RSSI = -RSSIVAL/2 dBm		*/

/* ----- IRQ and pin mapping register definitions -------------------------- */
#define RFM69_DIOCLKMAP		0x25	/* DIO, CLK out mapping control, (15:0) = MSB(0x25) + LSB(0x26)	*/
#define RFM69_DIOCLKMAPLEN	2

#define RFM69_IRQFLAGS		0x27	/* IRQ status flags, (15:0) = MSB(0x27) + LSB(0x28)	*/
#define RFM69_IRQFLAGSLEN	2

#define RFM69_RSSITHRSHLD	0x29	/* RSSI threshold, same coding as RSSI reading	*/
#define RFM69_RXTOSTRT		0x2A	/* Rx timeout relative to Rx start		*/
#define RFM69_RXTORSSI		0x2B	/* Rx timeout relative to RSSI threshold trigger	*/

/* ----- Packet engine register definitions -------------------------------- */
#define RFM69_PREAMBLE		0x2C	/* Preamble length, (15:0) = MSB(0x2C) + LSB(0x2D)	*/
#define RFM69_PREAMBLELEN	2

#define RFM69_SYNCCTRL		0x2E	/* Synchronization word control			*/

#define RFM69_SYNCVAL		0x2F	/* Synchronization word (63:0) = 8 bytes (0x2F) ... (0x36)	*/
#define RFM69_SYNCVALLEN	8	/* the max sync word length			*/

#define RFM69_PKTCFG1		0x37	/* Packet configuration 1 control, (7:1)	*/
#define RFM69_PKTSIZE		0x38	/* Packet/payload length			*/
#define RFM69_NADDR		0x39	/* Node address					*/
#define RFM69_BCADDR		0x3A	/* Broadcast address				*/
#define RFM69_AUTOCTRL		0x3B	/* Auto mode control				*/
#define RFM69_TXSTRTCTRL	0x3C	/* Tx start on FIFO fill condition control	*/
#define RFM69_PKTCFG2		0x3D	/* Packet configuration 2 control		*/

#define RFM69_AESKEY		0x3E	/* AES KEY, (127:0) = 16 byte (0x3E) ... (0x4D)	*/
#define RFM69_AESKEYLEN		16

/* ----- Temperature sensor register definitions --------------------------- */
#define RFM69_TEMPCTRL		0x4E	/* Temperature sensor control, (3:2)		*/
#define RFM69_TEMPVAL		0x4F	/* Temperature reading				*/

/* ----- Tuning register definitions --------------------------------------- */
#define RFM69_RXSENS		0x58	/* RX sensitivity, 0x1B normal / 0x2D high sensitivity	*/
#define RFM69_PLLBW		0x5F	/* PLL bandwidth				*/
#define RFM69_DAGC		0x6F	/* Digital AGC mode				*/
#define RFM69_AFCFOFF		0x71	/* AFC frequency offset on/off			*/

/* ----- Channel to Frequency lookup table --------------------------------- */
const u32 frequmult[] = {
/* CHN     10        11        12        13        14        15        16        17        18        19     */
	14173798, 14177075, 14180352, 14183629, 14186906, 14190182, 14193459, 14196736, 14200013, 14203290,
/* CHN     20        21        22        23        24        25        26        27        28        29     */
	14206566, 14209843, 14213120, 14216397, 14219674, 14222950, 14226227, 14229504, 14232781, 14236058,
/* CHN     00        01        02        03        04        05        06        07        08        09     */
	14141030, 14144307, 14147584, 14150861, 14154138, 14157414, 14160691, 14163968, 14167245, 14170522,
/* CHN     30        31        32        33        34     */
	14239334, 14242611, 14245888, 14249165, 14252442,
};

#define CHNCOUNT	sizeof(frequmult) / sizeof(u32)		/* Amount of Channel Numbers	*/

/* ----- Some initialization data ------------------------------------------ */
#if RFM69_HWTYPE == 0
#define MAXP	(0x60 | (MAXP_TX-MINP_TX))
#elif RFM69_HWTYPE == 1
#define MAXP	(0x80 | (MAXP_TX-MINP_TX))
#endif

#define PREL			4	/* Preamble length				*/

/* Initial setting summary
Operation Mode		STDBY			Standby mode, abort any pending Listen mode
Channel			32			default channel
PaRamp			10µ			shortest possible ramp
Preamble length		6			needs further optimization on Rx side to go down to 3
RSSIThreshold		-80dBm			to avoid rx reaction on spurs
DIO1mapping(Rx)		Timeout			to handle false reception
RSSITimeout		96 byte			sufficient for 6(PA)+4(Sync/LID)+4(RID)+60(PL) = 74
AFC			autoclear		AFC is started from scratch whenever Rx mode entered
AFC bandwidth		167kHz
Rx bandwidth		200kHz
*/
const u8 initregvect0[] = { RFM69_MODE | 0x80,
/*	  0     1     2     3     4     5     6     7     8     9     A     B     C     D     E     F	*/
/*00*/	      0x24, 0x02, 0x00, 0xD3, 0x02, 0x6D, 0xD9, 0x60, 0x00, 0x41, 0x00, 0x02, 0x92, 0xF5, 0x20,
/*10*/	0x24, MAXP, 0x0F, 0x1A, 0x40, 0xB0, 0x7B, 0x9B, 0x80, 0x49, 0x91, 0x40, 0x80, PREL, 0x0C, 0x00,
/*20*/	0x00, 0x00, 0x00, 0x02, 0xFF, 0x30, 0x07, 0x80, 0x00, 0xA0, 0x00, 0x30, 0x00, 0x08, 0x98, 0x7E,
/*30*/	0x0F, 0x0E, 0x32, 0xDE, 0xAD, 0xDA, 0x7A, 0x10, 0x40, 0x00, 0x00, 0x00, 0x80, 0x02, 0x00, 0x00,
/*40*/	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00,
};
const u8 initregvect1[] = { RFM69_RXSENS | 0x80,	0x1B, 0x09 };
const u8 initregvect2[] = { RFM69_PLLBW | 0x80,		0x08 };
const u8 initregvect3[] = { RFM69_DAGC | 0x80,		0x30 };
const u8 initregvect4[] = { RFM69_AFCFOFF | 0x80,	0x00 };

const u8 *initregvect[] = { initregvect0,		initregvect1,		initregvect2,		initregvect3,		initregvect4 };
const int initvectlen[] = { sizeof(initregvect0), 	sizeof(initregvect1), 	sizeof(initregvect2),	sizeof(initregvect3),	sizeof(initregvect4) };

#define INITTRNSFNBR	sizeof(initvectlen) / sizeof(int)	/* Number of elements in an array						*/

/* ----- RFM69 HW module initialization ------------------------------------ */
static void spi_hwinit_complete(void* context) {
	struct rfm69_object *rfm69obj;

	rfm69obj = spi_get_drvdata((struct spi_device*)context);
	rfm69obj->chiprev = rfm69obj->iobuf[RFM69_CHIPVER];
	printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : ... initialization finished. ChipID: 0x%02x.\n", rfm69obj->chiprev);
}

int rfm69_hwinit(struct spi_device* spidev) {
	int i, error = 0;
	struct rfm69_object* rfm69obj;
	u8 *rxbuffer;

	rfm69obj = spi_get_drvdata(spidev);
	rxbuffer = rfm69obj->iobuf;

	spi_message_init_with_transfers(rfm69obj->message,rfm69obj->transfers,INITTRNSFNBR);
	rfm69obj->message->spi = spidev;
	rfm69obj->message->complete = spi_hwinit_complete;
	rfm69obj->message->context = spidev;
	for (i = 0; i < INITTRNSFNBR; ++i) {
		rfm69obj->transfers[i].tx_buf = initregvect[i];
		rfm69obj->transfers[i].rx_buf = rxbuffer;
		rfm69obj->transfers[i].len = initvectlen[i];
		rfm69obj->transfers[i].cs_change = 1;
		rxbuffer += initvectlen[i];
	}
		rfm69obj->transfers[INITTRNSFNBR-1].cs_change = 0;

	error = spi_async(rfm69obj->spidev, rfm69obj->message);
	if (error) {
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : SPI submission for HW init failed.\n");
		goto Exit_error_0;
	}
	printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : HW Module initialization started ...\n");

Exit_error_0:
	return error;
}


/* ----- Transmit my Local ID together with some payload to Remote ID ------ */
static int irq_tx_plllock(struct rfm69_object *rfm69obj) {
	printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : Tx: PLL locked on entering/leaving Tx mode.\n");
	return 0;
}

static int irq_transmit_complete(struct rfm69_object *rfm69obj) {
	printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : Tx: LID(0x%08x) => RID(0x%08x) @ CHN(%02d) completed.\n", rfm69obj->localid, rfm69obj->remoteid, rfm69obj->chn);
	rfm69obj->dio[0].irqfunction = NULL;
	rfm69obj->dio[1].irqfunction = NULL;
	atomic_set(&rfm69obj->ready, READY);
	wake_up(&rfm69obj->waitqueue);
	return 0;
}

static void spi_transmit_issued(void* context) {
	struct rfm69_object *rfm69obj;

	rfm69obj = spi_get_drvdata((struct spi_device*)context);
	rfm69obj->dio[0].irqfunction = irq_transmit_complete;
	rfm69obj->dio[1].irqfunction = irq_tx_plllock;
	printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : Tx: LID(0x%08x) => RID(0x%08x) @ CHN(%02d) started.\n", rfm69obj->localid, rfm69obj->remoteid, rfm69obj->chn);
}

int rfm69_transmit(struct spi_device *spidev) {
	int lpv, error = 0;
	u32 freqc;
	u8 power; /* contents of register "RegPaLevel" */
	struct rfm69_object *rfm69obj;
	struct spi_transfer *transfers;
	u8* iobuffer;

	rfm69obj = spi_get_drvdata(spidev);

	freqc = frequmult[rfm69obj->chn];
	if (RFM69_HWTYPE == 1) /* 0 = RFM69HxW, 1 = RFM69xW */
		power = (rfm69obj->p_out + 18 /* dBm*/) | 0x80;
	else
		power = (rfm69obj->p_out + 14 /* dBm*/) | 0x60;
	iobuffer = rfm69obj->iobuf;
	transfers = rfm69obj->transfers;

	spi_message_init_with_transfers(rfm69obj->message, rfm69obj->transfers, 7/*6*/);
	rfm69obj->message->spi = spidev;
	rfm69obj->message->complete = spi_transmit_issued;
	rfm69obj->message->context = spidev;

	CCMDWR(RFM69_FREQC,freqc,RFM69_FREQCLEN,transfers,CONT,iobuffer,lpv);
	CCMDWR(RFM69_PKTSIZE,rfm69obj->dsize,RFM69_REGLEN,transfers,CONT,iobuffer,lpv);
	CCMDWR(RFM69_SYNCVAL,rfm69obj->remoteid,sizeof(rfm69obj->remoteid),transfers,CONT,iobuffer,lpv);
	CCMDWR(RFM69_MODE,0x04,RFM69_REGLEN,transfers,CONT,iobuffer,lpv); /* STDBY Mode as default mode */
	CCMDWR(RFM69_AUTOCTRL,0x3B,RFM69_REGLEN,transfers,CONT,iobuffer,lpv); /* AutoMode - entry on FifoNotEmpty, exit on PacketSent */
	CCMDWR(RFM69_PALEVEL,power,RFM69_REGLEN,transfers,CONT,iobuffer,lpv); /* Output power */
	iobuffer = rfm69obj->iobuf+DBID;
	WRBUF(rfm69obj->localid,iobuffer,sizeof(rfm69obj->localid),lpv);
	iobuffer = rfm69obj->iobuf+DBADDR;
	CFIFOWR(RFM69_FIFO,rfm69obj->dsize,transfers,LAST,iobuffer,lpv);

	error = spi_async(rfm69obj->spidev, rfm69obj->message);
	if (error) {
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : SPI submission for Tx failed.\n");
		goto Exit_error_0;
	}
Exit_error_0:
	return error;
}

/* ----- Receive a Remote ID on my Local ID as sync word ------------------- */
static void spi_receive_complete(void* context) {
	int lpv;
	struct rfm69_object *rfm69obj;
	u8* iobuffer;

	rfm69obj = spi_get_drvdata((struct spi_device*)context);
	iobuffer = rfm69obj->iobuf + DBID;
	RDBUF(iobuffer,rfm69obj->remoteid,sizeof(rfm69obj->remoteid),lpv);

	printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : Rx: LID(0x%08x) <= RID(0x%08x) @ CHN(%02d) completed.\n", rfm69obj->localid, rfm69obj->remoteid, rfm69obj->chn);
	rfm69obj->datavalid = 1;
	atomic_set(&rfm69obj->dataready, READY);
	wake_up(&rfm69obj->waitqueue);
}

static int irq_rxoff_plllock(struct rfm69_object *rfm69obj) {
	printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : Rx: PLL re-locked on leaving Rx mode.\n");
	rfm69obj->dio[1].irqfunction = NULL;
	return 0;
}

static int irq_receive_avail(struct rfm69_object *rfm69obj) {
	int lpv, error = 0;
	struct spi_transfer *transfers;
	u8* iobuffer;

	printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : Rx: LID(0x%08x) <= RID(0x????????) @ CHN(%02d) data available.\n", rfm69obj->localid, rfm69obj->chn);
	rfm69obj->dio[0].irqfunction = NULL;
	rfm69obj->dio[1].irqfunction = irq_rxoff_plllock; /* need to catch DIO1 IRQ caused by PLL locking (TODO: prove) when leaving Rx mode */

	iobuffer = rfm69obj->iobuf;
	transfers = rfm69obj->transfers;

	spi_message_init_with_transfers(rfm69obj->message,rfm69obj->transfers,2);
	rfm69obj->message->spi = rfm69obj->spidev;
	rfm69obj->message->complete = spi_receive_complete;
	rfm69obj->message->context = rfm69obj->spidev;

	CCMDWR(RFM69_MODE,0x04,RFM69_REGLEN,transfers,CONT,iobuffer,lpv); /* STDBY Mode */
	iobuffer = rfm69obj->iobuf+DBADDR;
	CCMDRD(RFM69_FIFO,rfm69obj->dsize,transfers,LAST,iobuffer,lpv);

	error = spi_async(rfm69obj->spidev, rfm69obj->message);
	if (error) {
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : SPI submission for Rx data read failed.\n");
		goto Exit_error_0;
	}
Exit_error_0:
	return error;
}

static void spi_receive_terminate(void* context) {
	struct rfm69_object *rfm69obj;

	rfm69obj = spi_get_drvdata((struct spi_device*)context);
	rfm69obj->datavalid = 0;
	atomic_set(&rfm69obj->dataready, READY);
	wake_up(&rfm69obj->waitqueue);
}

static int irq_rxrssi_timeout(struct rfm69_object *rfm69obj) {
	int lpv, error = 0;
	struct spi_transfer *transfers;
	u8* iobuffer;

	printk(KERN_NOTICE RFM69GMSK_DRIVER_NAME " : Rx: RSSI to PayLoad-ready timed out - terminating Rx mode.\n");
	rfm69obj->dio[0].irqfunction = NULL;
	rfm69obj->dio[1].irqfunction = irq_rxoff_plllock; /* need to catch DIO1 IRQ caused by PLL locking (TODO: prove) when leaving Rx mode */

	iobuffer = rfm69obj->iobuf;
	transfers = rfm69obj->transfers;

	spi_message_init_with_transfers(rfm69obj->message,rfm69obj->transfers,1);
	rfm69obj->message->spi = rfm69obj->spidev;
	rfm69obj->message->complete = spi_receive_terminate;
	rfm69obj->message->context = rfm69obj->spidev;

	CCMDWR(RFM69_MODE,0x04,RFM69_REGLEN,transfers,LAST,iobuffer,lpv); /* STDBY Mode */

	error = spi_async(rfm69obj->spidev, rfm69obj->message);
	if (error) {
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : SPI submission for Rx data read failed.\n");
		goto Exit_error_0;
	}
Exit_error_0:
	return error;
}

static int irq_rxon_plllock(struct rfm69_object *rfm69obj) {
	printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : Rx: PLL locked on entering Rx mode.\n");
	rfm69obj->dio[1].irqfunction = irq_rxrssi_timeout; /* as PLL has locked Rx mode will eventually be entered and we can start to ckeck for timeout */
	return 0;
}

static void spi_receive_issued(void* context) {
	struct rfm69_object *rfm69obj;

	rfm69obj = spi_get_drvdata((struct spi_device*)context);
	rfm69obj->dio[0].irqfunction = irq_receive_avail;
	rfm69obj->dio[1].irqfunction = irq_rxon_plllock; /* need to catch DIO1 IRQ caused by PLL locking when entering Rx mode */
	printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : Rx: LID(0x%08x) <= RID(0x????????) @ CHN(%02d) started.\n", rfm69obj->localid, rfm69obj->chn);
}

int rfm69_receive(struct spi_device *spidev) {
	int lpv, error = 0;
	u32 freqc;
	u8 rssithresh; /* contents of register "RegRssiThresh" */
	struct rfm69_object *rfm69obj;
	struct spi_transfer *transfers;
	u8* iobuffer;

	rfm69obj = spi_get_drvdata(spidev);

	freqc = frequmult[rfm69obj->chn];
	rssithresh = -2 * rfm69obj->p_rssi;
	iobuffer = rfm69obj->iobuf;
	transfers = rfm69obj->transfers;

	spi_message_init_with_transfers(rfm69obj->message,rfm69obj->transfers,6/*5*/);
	rfm69obj->message->spi = spidev;
	rfm69obj->message->complete = spi_receive_issued;
	rfm69obj->message->context = spidev;

	CCMDWR(RFM69_FREQC,freqc,RFM69_FREQCLEN,transfers,CONT,iobuffer,lpv);
	CCMDWR(RFM69_AUTOCTRL,0x00,RFM69_REGLEN,transfers,CONT,iobuffer,lpv); /* disable AutoMode */
	CCMDWR(RFM69_PKTSIZE,rfm69obj->dsize,RFM69_REGLEN,transfers,CONT,iobuffer,lpv);
	CCMDWR(RFM69_SYNCVAL,rfm69obj->localid,sizeof(rfm69obj->localid),transfers,CONT,iobuffer,lpv);
	CCMDWR(RFM69_RSSITHRSHLD,rssithresh,RFM69_REGLEN,transfers,CONT,iobuffer,lpv); /* RSSI threshold */
	CCMDWR(RFM69_MODE,0x10,RFM69_REGLEN,transfers,LAST,iobuffer,lpv); /* Rx Mode */

	error = spi_async(rfm69obj->spidev, rfm69obj->message);
	if (error) {
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : SPI submission for Rx failed.\n");
		goto Exit_error_0;
	}
Exit_error_0:
	return error;
}
