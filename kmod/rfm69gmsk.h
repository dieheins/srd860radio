/* rfm69gmsk.h - kernel module, configuration header file

Copyright (C) 2016  W. Hein, G. Kraut

This file is part of rfm69gmsk.

rfm69gmsk is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

rfm69gmsk is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rfm69gmsk.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __RFM69GMSK_H__
#define __RFM69GMSK_H__

/* ----- Definitions -------------------------------------------------------*/

/* RFM69 HW type */
typedef enum _hw_type_t {
	RFM69HxW	= 0,	/* Pmax = +20dBm */
	RFM69xW,		/* Pmax = +13dBm */
/* x can be either void or 'C', and C means compatible to RFM12B which is
 * here in any case irrelevant, i.e. don't care - the only relevant info for
 * tx power control is the module's Pmax indicated by the 'H'
*/
} hw_type_t;

#define RFM69_HWTYPE 		0	/* 0 = RFM69HxW, 1 = RFM69xW */

/* RX RSSI powers [dBm] */
#define MINP_RX	-127
#define MAXP_RX 0

/* TX output powers [dBm], depend on RFM69 module */
#if RFM69_HWTYPE == 0
#define MINP_TX	 2
#define MAXP_TX 17
#elif RFM69_HWTYPE == 1
#define MINP_TX	-18
#define MAXP_TX 13
#endif


/* Kernel module name */
#define RFM69GMSK_DRIVER_NAME	"rfm69gmsk"
#define CLASS_NAME		"rfm69"

/* RFM69 HW connection */
#define RFM69_SPI_BUS_NBR	0	/* header pins: 19/MOSI, 21/MISO, 23/SCLK */
#define RFM69_SPI_CS		0	/* header: pin 24/CE0 */
#define RFM69_SPI_CLK_MAX	7812500 /* 125MHz/16 */
#define RFM69_SPI_BITS		8
#define RFM69_SPI_MODE		SPI_MODE_0
#define RFM69_IRQ_NBR		2	/* number of interrupt lines from RF module */
#define RFM69_IRQ_DIO0		25	/* header: pin 22/GPIO25 */
#define RFM69_IRQ_DIO1		24	/* header: pin 18/GPIO24 */
#define RFM69_IRQ_TYPE		IRQF_TRIGGER_RISING

#define DEVNAMELEN		64	/* hopefully the max length of a spi device name */
#define MAXDEVOPENS		-1	/* max numer of device file opens (times -1 !!!) */
#define READY			-1	/* kernel module or data ready */
#define NOTREADY		0	/* kernel module or data not ready */

/* ----- Declarations ------------------------------------------------------*/

/* Data for module handling */
struct rfm69_object {
	wait_queue_head_t	waitqueue;
	atomic_t		devcounter;
	atomic_t		ready;
	atomic_t		dataready;
	bool			datavalid;
	struct	spi_master	*spimas;
	struct	spi_device	*spidev;
	struct	rfm69_int {
		int		gpio;
		int		irqnbr;
		int		(* irqfunction) (struct rfm69_object *);
	} dio[RFM69_IRQ_NBR];
	hw_type_t		hwtype;
	u8			chiprev;
	u32			localid;
	u32			remoteid;
	u8			dsize;
	u8			chn;
	int8_t			p_out;
	int8_t			p_rssi;
	struct spi_message	*message;
	struct spi_transfer	*transfers;
	u8			*iobuf;
};

#endif
