/* rfm69gmsk_core.c - kernel module, main part

Copyright (C) 2016  W. Hein, G. Kraut

This file is part of rfm69gmsk.

rfm69gmsk is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

rfm69gmsk is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rfm69gmsk.  If not, see <http://www.gnu.org/licenses/>. */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/spi/spi.h>
#include <linux/wait.h>
#include <linux/interrupt.h>
#include <asm/gpio.h>
#include <asm/uaccess.h>
#include <asm/atomic.h>

#include "linux/rfm69dev.h"
#include "rfm69gmsk.h"
#include "rfm69io.h"

/* ----- Driver's core data structure --------------------------------------*/

static struct rfm69_object rfm69obj = {
	.devcounter = ATOMIC_INIT(0),
	.ready = ATOMIC_INIT(NOTREADY),
	.dataready = ATOMIC_INIT(NOTREADY),
	.datavalid = 0,
	.dio[0] = {
		.gpio = RFM69_IRQ_DIO0,
	},
	.dio[1] = {
		.gpio = RFM69_IRQ_DIO1,
	},
	.localid = 0,
	.remoteid = 0,
};

/* ----- HW Module setup business ------------------------------------------*/
/* get the HW connection RFM69 <=> RasPi ready, i.e.:
   - SPI bus and chip select number, incl. some other SPI configurations
   - IRQ trigger line and its associated GPIO
   a static structure is defined to store this connection for later use
*/

static int rfm69_moduleaccess_setup(void) {
	int error = 0, i;
	struct device* testdev;
	char testdevname[DEVNAMELEN];

	/* setup SPI connection SPI-BUS <-> RFM69 */
	rfm69obj.spimas = spi_busnum_to_master(RFM69_SPI_BUS_NBR);
	if (rfm69obj.spimas == NULL) {
		error = -ENODEV;
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : SPI master access failed:.\n");
		goto Exit_error_0;
	}
	rfm69obj.spidev = spi_alloc_device(rfm69obj.spimas);
	if (rfm69obj.spidev == NULL) {
		error = -ENOMEM;
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : SPI slave setup failed:.\n");
		goto Exit_error_1;
	}
	/* before finally register device for this module check whether already registered and unregister it */    
	snprintf(testdevname,DEVNAMELEN,"%s.%u",dev_name(&rfm69obj.spimas->dev),RFM69_SPI_CS);
	testdev = bus_find_device_by_name(rfm69obj.spidev->dev.bus, NULL, testdevname);
	if (testdev != NULL) {
		printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : found a blocking SPI device <%s>.\n", testdevname);
		spi_unregister_device((struct spi_device*)testdev);
		spi_dev_put((struct spi_device*)testdev);
		testdev = bus_find_device_by_name(rfm69obj.spidev->dev.bus, NULL, testdevname);
		if (testdev != NULL ) {
			printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : SPI device <%s> is permanently blocking - giving up!\n", testdevname);
			error = -EBUSY;
			goto Exit_error_2;
		}
		else printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : blocking SPI device <%s> removed.\n", testdevname);
	}
	/* now register device */
	rfm69obj.spidev->max_speed_hz = RFM69_SPI_CLK_MAX;
	rfm69obj.spidev->chip_select = RFM69_SPI_CS;
	rfm69obj.spidev->bits_per_word = RFM69_SPI_BITS;
	rfm69obj.spidev->mode = RFM69_SPI_MODE;
	rfm69obj.spidev->irq = -1; /* not here: IRQ is done via GPIO */
	rfm69obj.spidev->controller_state = NULL;
	rfm69obj.spidev->controller_data = NULL;
	strlcpy(rfm69obj.spidev->modalias, RFM69GMSK_DRIVER_NAME, SPI_NAME_SIZE);

	error = spi_add_device(rfm69obj.spidev);
	if (error) {
		error = -EBUSY;
		goto Exit_error_2;
	}

	/* setup IRQ connection GPIO <-> RRM69 */
	for (i = 0; i < RFM69_IRQ_NBR; i++) {
		gpio_unexport(rfm69obj.dio[i].gpio); /* TODO: clarify whether needed */
		error = gpio_request(rfm69obj.dio[i].gpio, RFM69GMSK_DRIVER_NAME "-IRQ");
		if (error) {
			printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : GPIO_%2d request failed.\n", rfm69obj.dio[i].gpio);
			goto Exit_error_3;
		}
		error = gpio_direction_input(rfm69obj.dio[i].gpio);
		if (error) {
			printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : GPIO_%2d input request failed.\n", rfm69obj.dio[i].gpio);
			goto Exit_error_3;
		}
		rfm69obj.dio[i].irqnbr = gpio_to_irq(rfm69obj.dio[i].gpio);
		if (rfm69obj.dio[i].irqnbr == 0) {
			error = -EIO;
			printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : GPIO_%2d to IRQ assignment failed.\n", rfm69obj.dio[i].gpio);
			goto Exit_error_3;
		}
		rfm69obj.dio[i].irqfunction = NULL;
	}

	/* success */
	printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : SPI device and GPIO/IRQ registered.\n");
	goto Exit;

Exit_error_3:
	for (i = 0; i < RFM69_IRQ_NBR; i++)
		if (rfm69obj.dio[i].irqnbr)
			gpio_free(rfm69obj.dio[i].gpio);
	spi_unregister_device(rfm69obj.spidev);
Exit_error_2:
	spi_dev_put(rfm69obj.spidev);
Exit_error_1:
	put_device(&rfm69obj.spimas->dev);
Exit_error_0:
Exit:
	return error;
}

static int rfm69_moduleaccess_cleanup(void) {
	int i;
	for (i = 0; i < RFM69_IRQ_NBR; i++)
		gpio_free(rfm69obj.dio[i].gpio);
	spi_unregister_device(rfm69obj.spidev);
	spi_dev_put(rfm69obj.spidev);
	put_device(&rfm69obj.spimas->dev);
	return 0;
}

/* ----- Top level Interrupt Service routine ------------------------------- */
static irqreturn_t rfm69_isr_dio0(int irqnbr, void *irqcontext) {
	int irqres = 0;
	struct rfm69_object *rfm69obj = (struct rfm69_object*)irqcontext;

	if (rfm69obj->dio[0].irqfunction == NULL) {
		printk(KERN_NOTICE RFM69GMSK_DRIVER_NAME " : DIO0 IRQ w/o function assignment.\n");
		return IRQ_HANDLED;
	}
	irqres = (*rfm69obj->dio[0].irqfunction)(rfm69obj);
	return IRQ_HANDLED;
}

static irqreturn_t rfm69_isr_dio1(int irqnbr, void *irqcontext) {
	int irqres = 0;
	struct rfm69_object *rfm69obj = (struct rfm69_object*)irqcontext;

	if (rfm69obj->dio[1].irqfunction == NULL) {
		printk(KERN_NOTICE RFM69GMSK_DRIVER_NAME " : DIO1 IRQ w/o function assignment.\n");
		return IRQ_HANDLED;
	}
	irqres = (*rfm69obj->dio[1].irqfunction)(rfm69obj);
	return IRQ_HANDLED;
}

/* ----- SPI device probe and remove --------------------------------------- */
static int rfm69_device_probe(struct spi_device* spidev) {
	int error = 0, i;

	spi_set_drvdata(spidev, &rfm69obj);
	rfm69obj.hwtype = RFM69_HWTYPE;

	rfm69obj.message = spi_message_alloc(TRANSFERNBR, GFP_KERNEL);
	if (rfm69obj.message == NULL) {
		error = -ENOMEM;
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : SPI message setup failed.\n");
		goto Exit_error_0;
	}
	rfm69obj.transfers = (struct spi_transfer *)(rfm69obj.message + 1);

	rfm69obj.iobuf = kzalloc(IOBUFLEN, GFP_KERNEL);
	if (rfm69obj.iobuf == NULL) {
		error = -ENOMEM;
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : HW module iobuffer allocation failed.\n");
		goto Exit_error_1;
	}

	error = request_irq(rfm69obj.dio[0].irqnbr, rfm69_isr_dio0, IRQF_TRIGGER_RISING, RFM69GMSK_DRIVER_NAME, &rfm69obj);
	if (error) {
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : IRQ on DIO0 request failed.\n");
		goto Exit_error_2;
	}

	error = request_irq(rfm69obj.dio[1].irqnbr, rfm69_isr_dio1, IRQF_TRIGGER_RISING, RFM69GMSK_DRIVER_NAME, &rfm69obj);
	if (error) {
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : IRQ on DIO1 request failed.\n");
		goto Exit_error_2;
	}

	error = rfm69_hwinit(spidev);
	if (error) {
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : HW module initialization failed.\n");
		goto Exit_error_2;
	}

	goto Exit;

Exit_error_2:
	for (i = 0; i < RFM69_IRQ_NBR; i++)
		if (rfm69obj.dio[i].irqnbr)
			free_irq(rfm69obj.dio[i].irqnbr, &rfm69obj);
	kfree(rfm69obj.iobuf);
Exit_error_1:
	kfree(rfm69obj.message);
Exit_error_0:
	spi_set_drvdata(spidev, NULL);
Exit:
	return error;
}

static int rfm69_device_remove(struct spi_device* spidev) {
	int i;
	for (i = 0; i < RFM69_IRQ_NBR; i++)
		free_irq(rfm69obj.dio[i].irqnbr, &rfm69obj);
	kfree(rfm69obj.iobuf);
	kfree(rfm69obj.message);
	spi_set_drvdata(spidev, NULL);

	return 0;
}

/* ----- Character device business -----------------------------------------*/

static int chardev_open(struct inode *inode, struct file *file) {
	if(atomic_inc_not_zero(&rfm69obj.devcounter)) return 0;
	return -EBUSY;
}

static int chardev_release(struct inode *inode, struct file *file) {
	atomic_dec(&rfm69obj.devcounter);
	return 0;
}

static ssize_t chardev_read(struct file *file, char *buffer, size_t len, loff_t *offset) {
	int error = 0, size;

	size = len < rfm69obj.dsize ? 0 : rfm69obj.dsize;
	if (size) {
		if(wait_event_interruptible(rfm69obj.waitqueue, atomic_inc_not_zero(&rfm69obj.ready) != 0)) return -ERESTARTSYS;

		atomic_set(&rfm69obj.dataready, NOTREADY); /* just for extra safety */
		error = rfm69_receive(rfm69obj.spidev);

		if(wait_event_interruptible(rfm69obj.waitqueue, atomic_inc_not_zero(&rfm69obj.dataready) != 0)) return -ERESTARTSYS;

		if (rfm69obj.datavalid) {
			error = put_user(rfm69obj.remoteid, (u32 *)buffer);
			error = copy_to_user(buffer+sizeof(rfm69obj.remoteid), rfm69obj.iobuf+DBPL, size-sizeof(rfm69obj.remoteid));
		} else
			size = 0;

		atomic_set(&rfm69obj.dataready, NOTREADY);
		atomic_set(&rfm69obj.ready, READY);
		wake_up(&rfm69obj.waitqueue);

		*offset += size;
	}
	return size;
}

static ssize_t chardev_write(struct file *file, const char *buffer, size_t len, loff_t *offset) {
	int error = 0, size;

	size = len < rfm69obj.dsize ? 0 : rfm69obj.dsize;
	if (size) {
		if(wait_event_interruptible(rfm69obj.waitqueue, atomic_inc_not_zero(&rfm69obj.ready) != 0)) return -ERESTARTSYS;

		error = get_user(rfm69obj.remoteid, (u32 *)buffer);
		error = copy_from_user(rfm69obj.iobuf+DBPL, buffer+sizeof(rfm69obj.remoteid), size-sizeof(rfm69obj.remoteid));
		error = rfm69_transmit(rfm69obj.spidev);

		*offset += size;
	}
	return size;
}

static long chardev_ioctl(struct file *file, unsigned int ioctl_num, unsigned long ioctl_param) {
	int error = 0;
	int32_t ioctlval;

	if(wait_event_interruptible(rfm69obj.waitqueue, atomic_inc_not_zero(&rfm69obj.ready) != 0)) return -ERESTARTSYS;

	error = get_user(ioctlval, (int32_t *)ioctl_param);
	if (error) {
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : input IOCTL parameter failed.\n");
		goto Exit_error_0;
	}

	switch(ioctl_num) {
		case IOCTL_CHN:
			if (ioctlval > CHANNELS -1) {
				error = -EINVAL;
				printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : Setting Channel Number: %02d failed.\n", ioctlval);
				break;
			}
			rfm69obj.chn = ioctlval;
			printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : Channel Number set: %02d.\n", ioctlval);
			break;
		case IOCTL_LID:
			if ((ioctlval == 0) || (ioctlval == 0xFFFFFFFF)) {
				error = -EINVAL;
				printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : Setting Local ID: 0x%08x failed.\n", ioctlval);
				break;
			}
			rfm69obj.localid = ioctlval;
			printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : Local ID set: 0x%08x.\n", rfm69obj.localid);
			break;
		case IOCTL_DSIZE:
			if ((ioctlval < sizeof(rfm69obj.remoteid)) && (ioctlval > 64)) {
				error = -EINVAL;
				printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : Setting data block size: %d failed.\n", ioctlval);
				break;
			}
			rfm69obj.dsize = ioctlval;
			printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : Data block size set: %d.\n", rfm69obj.dsize);
			break;
		case IOCTL_POUT:
			if (ioctlval == 127) {	/* 127 indicates non-specified power */
				rfm69obj.p_out = MAXP_TX;
				printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : Output power not specified. Set to maximum power %d dBm.\n", rfm69obj.p_out);
				break;	
			}		
			if ((ioctlval < MINP_TX) || (ioctlval > MAXP_TX)) {	/* TX output power setting check */
				error = -EINVAL;
				printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : Output power %d dBm out of range [%d..%d] dBm.\n", ioctlval, MINP_TX, MAXP_TX);
				break;
			}
			rfm69obj.p_out = ioctlval;
			printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : TX output power set: %d dBm.\n", rfm69obj.p_out);
			break;
		case IOCTL_PRSSI:
			if (ioctlval == -128) {	/* -128 indicates non-specified power */
				rfm69obj.p_rssi = (MINP_RX - MAXP_RX)/2;
				printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : RSSI threshold level not specified. Set to average value %d dBm.\n", rfm69obj.p_rssi);
				break;	
			}	
			if ((ioctlval < MINP_RX) || (ioctlval > MAXP_RX)) {	/* RX RSSI threshold setting check */
				error = -EINVAL;
				printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : RSSI threshold power %d dBm out of range [%d..%d] dBm.\n", ioctlval, MINP_RX, MAXP_RX);
				break;
			}
			rfm69obj.p_rssi = ioctlval;
			printk(KERN_INFO RFM69GMSK_DRIVER_NAME " : RX RSSI level set: %d dBm.\n", rfm69obj.p_rssi);
			break;
		default:
			error = -EINVAL;
			printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : IOCTL 0x%08x unknown.\n", ioctl_num);
	}
Exit_error_0:
	atomic_set(&rfm69obj.ready, READY);
	wake_up(&rfm69obj.waitqueue);
	return error;
}

/* ----- Module business ---------------------------------------------------*/

static struct spi_driver rfm69gmsk_spi_driver = {
	.driver = {
		.name =		RFM69GMSK_DRIVER_NAME,
		.owner =	THIS_MODULE,
	},
	.probe =		rfm69_device_probe,
	.remove =		rfm69_device_remove,
};

static int chardevmajor;
static struct class* rmf69charClass = NULL;
static struct device* rfm69charDevice = NULL;

static struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = chardev_open,
	.release = chardev_release,
	.read = chardev_read,
	.write = chardev_write,
	.unlocked_ioctl = chardev_ioctl,
};

int __init rfm69gmsk_kmodule_init(void) {
	int error = 0;

	error = rfm69_moduleaccess_setup();
	if (error) goto Exit_error_0;

	error = spi_register_driver(&rfm69gmsk_spi_driver);
	if (error) goto Exit_error_1;

	chardevmajor = register_chrdev(0, DEVICE_NAME, &fops);
	if (chardevmajor < 0) {
		error = chardevmajor;
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : register major number failed.\n");
		goto Exit_error_2;
	}
 
	rmf69charClass = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(rmf69charClass)) {
		error = PTR_ERR(rmf69charClass);
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : register device class failed.\n");
		goto Exit_error_3;
	}

	rfm69charDevice = device_create(rmf69charClass, NULL, MKDEV(chardevmajor, 0), NULL, DEVICE_NAME);
	if (IS_ERR(rfm69charDevice)) {
		error = PTR_ERR(rfm69charDevice);
		printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : create device failed.\n");
		goto Exit_error_4;
	}

	init_waitqueue_head(&rfm69obj.waitqueue);
	atomic_set(&rfm69obj.dataready, NOTREADY);
	atomic_set(&rfm69obj.ready, READY);
	atomic_set(&rfm69obj.devcounter, MAXDEVOPENS);
	printk(KERN_NOTICE RFM69GMSK_DRIVER_NAME " : driver ready.\n");
	goto Exit;

Exit_error_4:
	class_destroy(rmf69charClass);
Exit_error_3:
	unregister_chrdev(chardevmajor, DEVICE_NAME);
Exit_error_2:
	spi_unregister_driver(&rfm69gmsk_spi_driver);
Exit_error_1:
	rfm69_moduleaccess_cleanup();
Exit_error_0:
	printk(KERN_ERR RFM69GMSK_DRIVER_NAME " : loading driver failed: %i.\n", error);
Exit:
	return error;
}

void __exit rfm69gmsk_kmodule_exit(void) {   
	device_destroy(rmf69charClass, MKDEV(chardevmajor, 0));
	class_destroy(rmf69charClass);
	unregister_chrdev(chardevmajor, DEVICE_NAME);
	spi_unregister_driver(&rfm69gmsk_spi_driver);
	rfm69_moduleaccess_cleanup();
	printk(KERN_NOTICE RFM69GMSK_DRIVER_NAME " : driver ejected.\n");
}

module_init(rfm69gmsk_kmodule_init);
module_exit(rfm69gmsk_kmodule_exit);

MODULE_DESCRIPTION("SPI protocol driver for RF TRX module RFM69 running in GMSK");
MODULE_SUPPORTED_DEVICE("RFM69 W/HW/CW/HCW");
MODULE_AUTHOR("W. Hein, G. Kraut");
MODULE_LICENSE("GPL v2");
MODULE_VERSION("0.0.1");
