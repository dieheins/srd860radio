RFM69GMSK Kernel Module for Raspberry Pi
========================================

# Purpose

The RFM69GMSK Kernel Module runs a RFM69(H)CW radio transceiver module attached to Raspberry Pi's SPI bus
- in European Short Range Device Band 863 - 870 MHz (SRD860)
- in modulation scheme GMSK
- at approximately 150 kbps
- on one out of 35 channels of 200 kHz bandwidth each.

An enclosed user program demonstrates how data packets shall be handed over to kernel module for transmission
and how the kernel module is triggered to receive a data packet and finally how it returns it back to user program.
This user program shall just act as an example code for more advanced designs.

# Kernel Module: rfm69gmsk.ko

The kernel module `rfm69gmsk.ko`can be configured via `ioctl` functions and data I/O is performed from user space via
device file access on `/dev/rfm69`.

## RFM69HCW versus RFM69CW

There are 2 transceiver module hardware versions available.
Prior to compilation constant `RFM69_HWTYPE` needs to be set in file `rfm69gmsk.h` dependent on module's hardware version.

## IOCTL

All `ioctl` functions transfer a signed 32 bit value into module to configure

| IOCTL   | Description        | value range              | Comments                                                          |
| :------ | :----------------- | :----------------------: | :---------------------------------------------------------------- |
| `CHN`   | channel number     | 0 ... 34                 | number of the 200 kHz wide radio channel for Rx/Tx                |
| `LID`   | local ID           | u32                      | local identification number aka local station's address           |
| `DIZE`  | data block size    | 4 ... 64                 | size in octets of the Rx/Tx data block, includes 4 octets for RID |
| `POUT`  | tx power level     | +2 ... +17 / -18 ... +13 | transmit power [dBm] for either RFM69HCW or RFM69CW               |
| `PRSSI` | rx power threshold | -127 ... 0               | reception threshold power level [dBm]                             |

## Data input/output

Before starting a reception or transmission at least `CHN`, `LID` and `DIZE` have to be configured accordingly.
Actual data blocks of size `DSIZE` can then be exchanged via `/dev/rfm69`.
The data block is headed by 4 octets including the RID, the remote ID in hex code, aka remote station's address.
The remaining octets are then user data.

# User Program: trx

The user program `trx` can be used mutually exclusive in 2 ways, either to trigger a reception (Rx) or to perform a transmission (Tx).

## Reception (Rx)

A reception is triggered by

### trx rx -r _PRSSI_ _CHN_ _0xLID_

The RID, remote ID, of the sender will be reported upon reception.

## Transmission (Tx)

A transmission is initiated by 

### trx tx -p _POUT_ _CHN_ _0xLID_ _0xRID_

LID and RID are 4 octed hex codes for local and remote ID.