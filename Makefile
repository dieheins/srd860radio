SHELL := /bin/bash

all:
	cd kmod; make
	cd userprog; make

clean:
	cd kmod; make clean
	cd userprog; make clean

install:
	cp -a userprog/trx /usr/local/bin/
	cp -a lib/50-rfm69gmsk.rules /etc/udev/rules.d
	chown root:root /usr/local/bin/trx /etc/udev/rules.d/50-rfm69gmsk.rules
	if [ -n "$$(lsmod | grep rfm69gmsk)" ]; then rmmod rfm69gmsk; fi
	insmod kmod/rfm69gmsk.ko
	chown root:root /usr/local/bin/trx
