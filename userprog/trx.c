/* trx.c - simple user program

Copyright (C) 2016  W. Hein, G. Kraut

This file is part of rfm69gmsk.

rfm69gmsk is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

rfm69gmsk is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rfm69gmsk.  If not, see <http://www.gnu.org/licenses/>. */

#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include "linux/rfm69dev.h"

#define DEVICE_FILE_NAME	"/dev/"DEVICE_NAME

int main(int argc, char* argv[]) {
	int size, file, trx = 0, error = 0, c, argno;
	uint32_t chn, lid, rid;
	struct io_object {
		uint32_t id;
		uint16_t pl;
	} ioobj;
	int32_t p=127, r=-128; /* TX power and RSSI level, both [dBm]. 127 and -128 indicate that powers have not been specified yet on command line. */
	char *ap, *end;
	time_t t;

	/* initialize the random generator */
	time(&t);
	srand((unsigned int)t);

	/* Optional command line parameters */
	opterr = 0;
	while ((c = getopt (argc, argv, "p:r:")) != -1) {
		switch(c) {		
			case 'p':
				p = (int32_t)strtol(optarg, &end, 10);
				if (end == optarg) {    /* Check conversion failure. If no characters were converted these pointers are equal */
					fprintf(stderr, "Option -p: can't convert power string to number\n");
					return(-1);
				}
				break;
			case 'r':
				r = (int32_t)strtol(optarg, &end, 10);
				if (end == optarg) {     /* Check conversion failure. If no characters were converted these pointers are equal */
					fprintf(stderr, "Option -r: can't convert power string to number\n");
					return(-1);
				}
				break;
			case '?':
				if ((optopt == 'p') || (optopt == 'r'))
					fprintf (stderr, "Option -%c requires an argument.\n", optopt);
				else if (isprint (optopt))
					fprintf (stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
				return(-1);
		}
	}

	/* Non-optional command line parameters */
	argno = argc - optind + 1;
	switch(argno) {
		case 5:
			if(!strcmp(argv[optind], "tx")) {
				trx=1;
				chn = (uint32_t)atoi(argv[optind+1]);
				lid = (uint32_t)strtoul(argv[optind+2], &ap, 16);
				rid = (uint32_t)strtoul(argv[optind+3], &ap, 16);
			} else goto fault;
			break;
		case 4:
			if(!strcmp(argv[optind], "rx")) {
				trx=0;
				chn = (uint32_t)atoi(argv[optind+1]);
				lid = (uint32_t)strtoul(argv[optind+2], &ap, 16);
				rid = -1;
			} else goto fault;
			break;
		fault:
		default:
			printf("\n\n");
			printf("Usage(Tx): %s tx [-p P_out] ChNbr 0x4bytelocalID 0x4byteremoteID\n", argv[0]);
			printf("Usage(Rx): %s rx [-r P_RSSI] ChNbr 0x4bytelocalID\n", argv[0]);
			printf("\n\n");
			return(-1);
	}

	/*printf("%d, %08x, %d dBm.\n", chn, lid, p);*/

	file = open(DEVICE_FILE_NAME, O_RDWR);
	if (file < 0) {
		printf ("Can't open device file: %s\n", DEVICE_FILE_NAME);
		return (-1);
	}

	error = ioctl(file, IOCTL_CHN, &chn);
	if (error) {
		printf ("Can't set channel: %d.\n", chn);
		return errno;
	}

	error = ioctl(file, IOCTL_LID, &lid);
	if (error) {
		printf ("Can't set local ID: 0x%08x.\n", lid);
		return errno;
	}

	if (trx) {
		error = ioctl(file, IOCTL_POUT, &p);
		if (error) {
			printf ("Can't set TX power: %d dBm.\n", p);
			return errno;
		}
	}
	else {
		error = ioctl(file, IOCTL_PRSSI, &r);
		if (error) {
			printf ("Can't set RX RSSI level: %d dBm.\n", p);
			return errno;
		}
	}

	size = sizeof(ioobj);
	error = ioctl(file, IOCTL_DSIZE, &size);
	if (error) {
		printf ("Can't set data block size: %d.\n", size);
		return errno;
	}

	if(trx) {
		ioobj.id = rid;		/* set the target id */
		ioobj.pl = rand();	/* prepare payload for transmission */
		error = write(file, &ioobj, sizeof(ioobj));
		if (error != sizeof(ioobj)) {
			printf("Trigger transmission failed: %d.\n", error);
			return errno;
		}
		printf("Transmission 0x%08x => 0x%08x on CHN = %02d with Payload = 0x%04x.\n", lid, ioobj.id, chn, ioobj.pl);
	}
	else {
		error = read(file, &ioobj, sizeof(ioobj));
		if (error != sizeof(ioobj)) {
			printf("Trigger reception failed: %d.\n", error);
			return errno;
		}
		printf("Reception 0x%08x <= 0x%08x on CHN = %02d with Payload = 0x%04x.\n", lid, ioobj.id, chn, ioobj.pl);
	}

	close(file);
	return 0;
}
