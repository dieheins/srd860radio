/* rfm69dev.h - kernel module, userland header file

Copyright (C) 2016  W. Hein, G. Kraut

This file is part of rfm69gmsk.

rfm69gmsk is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

rfm69gmsk is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rfm69gmsk.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __RFM69DEV_H__
#define __RFM69DEV_H__

/* ----- Definitions -------------------------------------------------------*/

/* IOCTLs */
#define IOCTL_CHN	0x00046900	/* set channel number in range < 0, 1, ..., 33, 34 >	*/
#define IOCTL_LID	0x00046901	/* set local ID, i.e. my own ID	- 32 bit		*/
#define IOCTL_DSIZE	0x00046902	/* set rx/tx data block size from set < 4, ... 64 >	*/
#define IOCTL_POUT	0x00046903	/* set TX output power [dBm] */
#define IOCTL_PRSSI	0x00046904	/* set RX RSSI level [dBm] */

/* Device file name */
#define DEVICE_NAME 	"rfm69"		/* found under /dev/<DEVICE_NAME>			*/

/* Some numbers */

#endif
